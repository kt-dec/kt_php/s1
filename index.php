<?php require_once "./code.php"?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>s01: PHP Basics and Selections</title>
	</head>
	<body>
		<!-- <h1>Hellow Wurld?</h1> -->

		<h1>Echoing Values</h1>
		<!-- Variables can be used to output data in double quotes while single quotes do not. -->

		<p>
			<?php echo 'Good day $name! Your given email is $email' ?>
		</p>

		<p>
			<?php echo "Good day $name! Your given email is $email" ?>
		</p>

		<p>
			<?php echo PI ?>
		</p>
	<h1>Data Types</h1>
		<p>
			<?php echo "$address"?>
			<?php echo "$age"?>
		</p>

	<!-- To output the value of an object property, the arrow notation can be used -->

		<p>
			<?php echo $gradesObj->firstGrading; ?>
			<?php echo $personObj->address->state; ?>
		</p>

	<!-- Normal echoing of null and boolean var will not make it visible to the web page -->
		<p>
			<?php echo $hasTravelledAbroad; ?>
		</p>

		<p>
			<?php echo $girlfriend; ?>
		</p>

	<!-- var_dump see more details info on the variable -->
		<p><?php echo gettype($hasTravelledAbroad); ?></p>
		<p><?php echo var_dump($hasTravelledAbroad); ?></p>
		<p><?php echo var_dump($girlfriend); ?></p>

		<p><?php echo $grades[2]; ?>

	<h1>Operators</h1>
	
		<h2>Arithmetic Operators</h2>

			<p>Sum: <?php echo $x + $y; ?></p>

		<h2>Equality Operators</h2>

			<p>Loose Equality: <?php echo var_dump($x == 1342.14); ?></p>
			<p>Loose Equality: <?php echo var_dump($x != 1342.14); ?></p>

		<h2>Greater/Lesser Operator</h2>

			<p>Loose Equality: <?php echo var_dump($x < $y); ?></p>

		<h2>Logical Operator</h2>

			<p>Are All Requirements Met: <?php echo var_dump($isLegalAge && $isRegistered); ?></p>
			<p>Are All Requirements Met: <?php echo var_dump(!$isLegalAge && !$isRegistered); ?></p>

	<h1>Function</h1>
		
		<p>Full Name: <?php echo getFullName('John', 'D.', 'Smith'); ?></p>

		<h2>If-Elseif-Else</h2>
		<p><?php echo determineTyphoonIntensity(35); ?></p>

		<h2>Ternary</h2>
		<p>78: <?php var_dump(isUnderAge(78)); ?></p>

		<h2>Switch</h2>
		<p><?php echo determineComputerUser(4); ?></p>

		<h2>Try-Catch-Finally</h2>
		<p><?php echo greeting(4); ?></p>

	<h1>Activity</h1>

		<h2>Activity 1</h2>
		<p><?php echo getFullAddress("Philippines", "Quezon City", "Metro Manila", "Caswynn Building, 3rd Floor, 134 Timog Ave"); ?></p>
		
		<p><?php echo getFullAddress("Philippines", "Makati City", "Metro Manila", "Enzo Building, 3rd Floor, Buendia Avenue"); ?></p>

		<h2>Activity 2</h2>
		<p><?php echo getLetterGrade(87); ?></p>

		<p><?php echo getLetterGrade(94); ?></p>

		<p><?php echo getLetterGrade(74); ?></p>

		<p><?php echo getLetterGrade(99); ?></p>


	</body>
</html>